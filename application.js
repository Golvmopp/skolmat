$(document).ready(function() {
	var xhr = new XMLHttpRequest();
	xhr.open("Get", "http://meny.dinskolmat.se/api/openmeal/v2/meals.json?distributorID=5375932047032320", false);
	xhr.send();

	$.getJSON("http://meny.dinskolmat.se/api/openmeal/v2/meals.json?distributorID=5375932047032320", function(data)
	{
		var meatArray = ["kyckling", "korv", "kött", "lamm", "fläsk", "kotlett"];
		var fishArray = ["fisk", "lax", "strömming", "torsk", "sej"];
		var laktosArray = ["mjölk", "ost", "fil"];


		//$("#result").text(JSON.stringify(data, null, 2));

		for(var course in data.data[0].meals)
		{
			var date = data.data[0].meals[course].date;
			date = date.slice(0,10);

			var dateElement = document.createElement("h3");
			var node = document.createTextNode(date);
			dateElement.appendChild(node);

			var element = document.getElementById("result");
			element.appendChild(dateElement);

			for(var meal in data.data[0].meals[course].courses) {
				if(data.data[0].meals[0].courses.hasOwnProperty(meal)) {
					
					var specificMeal = data.data[0].meals[course].courses[meal].name;

					var meal = document.createElement("p");
					var node = document.createTextNode(specificMeal);
					meal.appendChild(node);

					//check meal
					var tempMeal = specificMeal.toLowerCase();
					for(var i = 0; i < meatArray.length; i++)
					{
						if(tempMeal.indexOf(meatArray[i]) >= 0)
						{
							meal.style.color = "blue";
						}
					}
					
					for(var i = 0; i < fishArray.length; i++)
					{
						if(tempMeal.indexOf(fishArray[i]) >= 0)
						{
							meal.style.color = "green";
						}
					}
					
					for(var i = 0; i < laktosArray.length; i++)
					{
						if(tempMeal.indexOf(laktosArray[i]) >= 0)
						{
							meal.style.color = "red";
						}
					}

					var element = document.getElementById("result");
					element.appendChild(meal);
				}
			}
		}

	}, "json");
});