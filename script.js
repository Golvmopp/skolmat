// jQuery.getJSON( url [, data ] [, success ] )
//   Returns: jqXHR
//   Description: Load JSON-encoded data from
//   the server using a GET HTTP request.

$.getJSON("http://meny.dinskolmat.se/api/openmeal/v2/meals.json?distributorID=5375932047032320", function(data)
 {

	// $("#result").text(JSON.stringify(data, null, 2));


	// Note that JSON is JavaScript Object Notation,
	//  so litteral JSON in javascript source is just a JS Object.

	// JSON.parse
	// ........................................................................
	// The JSON.parse() method parses a JSON string,
	//   constructing the JavaScript value or object
	//   described by the string. An optional reviver
	//   function can be provided to perform a transformation
	//   on the resulting object before it is returned.
	//
	// JSON.parse() is used to convert a string containing JSON notation
	//   into a Javascript object.
	//
	//   See http://stackoverflow.com/a/19239384
	//   and http://jsfiddle.net/NrnK5/ for example/explanation

	// JSON.stringify
	// ........................................................................
	// The JSON.stringify() method converts a JavaScript value to
	//   a JSON string, optionally replacing values if a replacer function
	//   is specified, or optionally including only the specified properties
	//   if a replacer array is specified.

	//  RESULT 0
	var obj = data;
	var result0 = obj;
	var result0datatype = typeof result0;


	//  RESULT 1
	var myMeals = obj.data;
	var result1 = JSON.stringify(myMeals);
	var result1datatype = typeof result1;


	//  RESULT 2
	var menuOfDay = myMeals[0].meals[0];
	var result2 = JSON.stringify(menuOfDay);
	var result2datatype = typeof result2;


	//  RESULT 3
	var courses = myMeals[0].meals[0].courses;
	var result3 = courses[0].name;
	var result3datatype = typeof result3;


	$("#result0").text(result0);
	$("#result0datatype").text(result0datatype);

	$("#result1").text(result1);
	$("#result1datatype").text(result1datatype);

	$("#result2").text(result2);
	$("#result2datatype").text(result2datatype);

	$("#result3").text(result3);
	$("#result3datatype").text(result3datatype);

	for(var i = 0; i < myMeals.length; i++) {
		var menuOfDay = myMeals[i].meals[i];
		for (courses in menuOfDay) {
			var course = menuOfDay[courses];
			console.log('course: ' + course);
		}
	}

}, "json");